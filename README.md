# FoundryVTT lang fr-FR

EN : 
		This module adds the option to select the French (FRANCE) language from the FoundryVTT settings menu. Selecting this option will translate various aspects of the program interface.

		Installation
				In the Add-On Modules option click on Install Module and place the following link in the field Manifest URL
				https://gitlab.com/baktov.sugar/foundryvtt-lang-fr-fr/raw/master/fr-FR/module.json
				If this option does not work download the fr-FR.zip file and extract its contents into the /resources/app/public/modules/ folder
				Once this is done, enable the module in the settings of the world in which you intend to use it and then change the language in the settings.

FR : 
		Ce module traduit les cl�s (termes) expos�s par Atropos pour le programme FoundryVTT. 
		
		Installation
			Dans le menu des modules, cliquer sur install module" et dans le champs Manifest, copier le lien ci-dessous
			https://gitlab.com/baktov.sugar/foundryvtt-lang-fr-fr/raw/master/fr-FR/module.json
			Une fois votre Monde lanc�, n'oubliez pas d'activer le module et de d�finir la langue Fran�aise pour ce monde.	
			
NOTE : Thx to Bellenus#5269 , Miriadis & Thomaz M. for initial copy of this module and yml : https://gitlab.com/elvis-pereira/foundryvtt-brazilian-portuguese
